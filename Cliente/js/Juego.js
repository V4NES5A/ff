let juego = {
    id: 0
}

function setIdJuego(id) {
    juego.id = id
}

$(document).ready(inicio);

//FUNCION INICIO
function inicio() {
    cargarDatos();
    $("#btnGuardar").click(guardar);
    $("#btnEliminar").click(function () {
        eliminar(juego.id)
    });
    $("#btnActualizar").click(modificar);
    $("#btnCancelar").click(reset);
}

function reset() {
    $("#nombre").val(null);
    $("#descripcion").val(null);

    $("#nombre2").val(null);
    $("#descripcion2").val(null);
}

//CARGANDO DATOS A TABLA ESPECIALIDADES
function cargarDatos() {
    $.ajax({
        url: "http://localhost:8081/juegos/all",
        method: "Get",
        data: null,
        success: function (response) {
            $("#datos").html("");

            for (let i = 0; i < response.length; i++) {
                $("#datos").append(
                    "<tr>" +
                    "<td><strong>" + response[i].id + "</strong></td>" +
                    "<td><strong>" + response[i].nombre + "</strong></td>" +
                    "<td><strong>" + response[i].descripcion + "</strong></td>" +
                    "<td><strong>" + response[i].categoria + "</strong></td>" +
                    "<td>" +
                    "<button onclick='cargarRegistro(" + response[i].id +
                    ")'type='button' class='btn btn-warning ml-3 mt-1' data-toggle='modal' data-target='#editar'><i class='fas fa-edit'></i> <strong>Editar</strong></button>" +
                    "<button onclick='setIdJuego(" + response[i].id +
                    ");' type='button' class='btn btn-danger ml-3 mt-1' style='color: black' data-toggle='modal' data-target='#eliminar'><i class='fas fa-trash-alt'></i> <strong>Eliminar</strong></button>" +
                    "</td>" +
                    "</tr>"
                )
            }
        },
        error: function (response) {
            alert("Error: " + response);
        }
    });
}

function guardar(response) {
    $.ajax({
        url: "http://localhost:8081/juegos/saveOrUpdate",
        method: "Put",
        data: {
            id: null,
            nombre: $("#nombre").val(),
            descripcion: $("#descripcion").val(),
            categoria: $("#categoria").val()
        },
        success: function () {
            //SuccessAlert();
            cargarDatos();            
            reset();
            
        },
        error: function (response) {
            
            alert("Error en la peticion: " + response)
        }
    })
}

function eliminar(id) {
    $.ajax({
        url: "http://localhost:8081/juegos/" + id,
        method: "Delete",
        success: function () {
            deleteAlert();
            cargarDatos();
        },
        error: function (response) {
            alert("Error en la peticion: " + response)
        }
    })
}

function cargarRegistro(id) {
    $.ajax({
        url: "http://localhost:8081/juegos/" + id,
        method: "Post",
        success: function (response) {
            $("#id2").val(response.id)
            $("#nombre2").val(response.nombre)
            $("#descripcion2").val(response.descripcion)
            $("#categoria2").val(response.categoria)
        },
        error: function (response) {
            alert("Error en la peticion " + response);
        }
    })
}

function modificar() {
    var id = $("#id2").val();
    $.ajax({
        url: "http://localhost:8081/juegos/saveOrUpdate/",
        method: "put",
        data: {
            id: id,
            nombre: $("#nombre2").val(),
            descripcion: $("#descripcion2").val(),
            categoria: $("#categoria2").val()
        },
        success: function (response) {
            console.log(response);
            modAlert();
            cargarDatos();
            reset();
        },
        error: function (response) {
            alert("Error en la peticion: " + response);
            console.log(id);
        }
    });
}

//funcion de sweet alert
function SuccessAlert(){
    Swal.fire({
        position: 'center',
        icon: 'success',
        title: 'El registro guardado correctamente',
        showConfirmButton: false,
        timer: 2000
      })
}
//funcion de sweet alert
function modAlert(){
    Swal.fire({
        position: 'center',
        icon: 'success',
        title: 'El registro actualizado correctamente',
        showConfirmButton: false,
        timer: 2000
      })
}

//funcion de sweet alert
function deleteAlert(){
    Swal.fire({
        position: 'center',
        icon: 'success',
        title: 'El registro ha sido eliminado correctamente',
        showConfirmButton: false,
        timer: 2000
      })
}

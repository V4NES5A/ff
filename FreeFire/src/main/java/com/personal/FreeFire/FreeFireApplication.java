package com.personal.FreeFire;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FreeFireApplication {

	public static void main(String[] args) {
		SpringApplication.run(FreeFireApplication.class, args);
	}

}

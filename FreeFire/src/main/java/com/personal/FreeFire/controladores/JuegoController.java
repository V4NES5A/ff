package com.personal.FreeFire.controladores;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.personal.FreeFire.entidades.Juego;
import com.personal.FreeFire.entidades.Juego.Categoria;
import com.personal.FreeFire.repositorios.IJuegoRepository;

@Controller
//nombre con el que se le reconoce al controlador
@RequestMapping("juegos")
//notacion para que la aplicacion sea consumida de forma externa
@CrossOrigin
public class JuegoController {
	@Autowired
	IJuegoRepository rJuego;
	
	
	 // LISTAR
    @GetMapping(value = "all", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<Juego> getAll() {
        return (List<Juego>) rJuego.findAll();
    }
    
 // GUARDAR
    @PutMapping(value = "saveOrUpdate", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    @CrossOrigin
    public HashMap<String, String> save(@RequestParam Long id,@RequestParam String nombre, @RequestParam String descripcion,@RequestParam Categoria categoria) {

        Juego juego = new Juego();
        
        System.out.println(id);
        if (id != null) {
            juego = new Juego(id, nombre, descripcion, categoria);
        } else if (id == null) {
            id = rJuego.count() + 3;
            juego = new Juego(id, nombre, descripcion, categoria);
        }

        HashMap<String, String> jsonReturn = new HashMap<>();

        try {
            rJuego.save(juego);

            jsonReturn.put("Estado", "OK");
            jsonReturn.put("Mensaje", "Registro guardado");

            return jsonReturn;
        } catch (Exception e) {

            jsonReturn.put("Estado", "Error");
            jsonReturn.put("Mensaje", "Registro no guardado");

            return jsonReturn;
        }
    }
    @PostMapping(value = "{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Juego getMethodName(@PathVariable Long id) {
        return rJuego.findById(id).get();
    }

    /** Metodo_para_eliminar_registro */
    @DeleteMapping(value = "{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Boolean delete(@PathVariable Long id) {
        Juego e = rJuego.findById(id).get();
        rJuego.delete(e);
        return true;
    }
}
package com.personal.FreeFire.entidades;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;


/*
 id INT PK
 nombre VARCHAR(50)
 descripcion VARCHAR(50)
 categoria enum('A','B','C')
 */

@Entity
@Table(name="juegos")
public class Juego {
	
	public enum Categoria{
		A,B,C
	}
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@NotNull(message="El campo nombre es obligatorio")
	@NotBlank(message="El campo nombre es obligatorio")
	private String nombre;
	
	@NotNull(message="El campo descripcion es obligatorio")
	@NotBlank(message="El campo descripcion es obligatorio")	
	private String descripcion;
	
	//notacion para cambiar un enum a char
	@Enumerated(EnumType.STRING)
	@Column(nullable=false, length = 30, columnDefinition = "ENUM('A','B','C')")
	private Categoria categoria;

	public Juego() {
		// TODO Auto-generated constructor stub
	}

	public Juego(Long id,
			@NotNull(message = "El campo nombre es obligatorio") @NotBlank(message = "El campo nombre es obligatorio") String nombre,
			@NotNull(message = "El campo descripcion es obligatorio") @NotBlank(message = "El campo descripcion es obligatorio") String descripcion,
			Categoria categoria) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.descripcion = descripcion;
		this.categoria = categoria;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Categoria getCategoria() {
		return categoria;
	}

	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}	
}

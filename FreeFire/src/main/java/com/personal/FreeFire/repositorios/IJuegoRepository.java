package com.personal.FreeFire.repositorios;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.personal.FreeFire.entidades.Juego;

@Repository
public interface IJuegoRepository extends CrudRepository<Juego, Long>{

}
